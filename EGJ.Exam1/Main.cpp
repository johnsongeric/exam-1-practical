
// Exam 1 - Practical
// Eric Johnson

#include <iostream>
#include <conio.h>

using namespace std;

void Square(float number, float &answer)
{
	answer = number * number;
}

void Cube(float number, float& answer)
{
	answer = number * number * number;
}

int main()
{
	
	int iContinue = 0;
	do
	{
		float input; // User inputs a number
		int iSelection; // User menu selection
		float answer = 0;

		cout << "Please enter a number: ";
		cin >> input;

		do // Check for a valid selection
		{
			cout << "\n";
			cout << "Enter a 2 for SQUARE or 3 for CUBE: ";
			cin >> iSelection;

			if (iSelection == 2 || iSelection == 3)
			{
				// If valid, continue
				iContinue = 1;
			}
			else
			{
				cout << "\n" << "Invalid Input, Please enter a 2 or a 3";
			}
		} while (iContinue == 0);
		iContinue = 0; // Reset varable to reuse again
		
		if (iSelection == 2)
		{
			Square(input, answer);
		}
		else
		{
			Cube(input, answer);
		}
		cout << "The answer is: " << answer;

		cout << "\n" << "Would you like to perform another calculation?" << "\n" << "0 = Yes, 1 = No" << "\n";
		cin >> iContinue;
	} while (iContinue == 0);
	return 0;
}
